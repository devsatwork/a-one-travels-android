package aonetravels.devsatwork.com.aonetravels;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by supreet on 3/5/2015.
 */
public class apiUtility {
    static HttpClient httpClient = new DefaultHttpClient();
    static HttpGet GetRequest;
    static HttpPost postRequest;
    private static HttpResponse response_get;
    private static HttpEntity getResponseEntity;
    HttpPost PostRequest;
    static final String base_url= "http://10.0.2.2:8000/api/v1/";
    static final String host_url= "http://10.0.2.2:8000";
    HttpResponse response;
    Boolean Success;
    String error_msg;
    String data;
    Context context;
    SharedPreferences pref;
    SharedPreferences.Editor prefEditor;

    public apiUtility(Boolean Success,HttpResponse response,String error_msg,String data,Context context){
        this.Success=Success;
        this.response=response;
        this.data=data;
        this.error_msg= error_msg;
        this.context=context;
    }
    public static apiUtility executeApi(String Type, String Url, List<NameValuePair> nameValuePair, Context context){
        apiUtility resp_object=new apiUtility(false,null,null,"",context);
        SharedPreferences sharedPref;
        String token;
        switch(Type){
            case "GET":
                Log.d("Url",base_url+Url);
                GetRequest=new HttpGet(base_url+Url);
                sharedPref = context.getSharedPreferences("session_token", Context.MODE_PRIVATE);
                token = sharedPref.getString("token","");
                Log.d("token",token);
                try {
//                    GetRequest.addHeader("X-Csrf-Token",token);
                    resp_object.response = httpClient.execute(GetRequest);
                    BufferedReader in = new BufferedReader(new InputStreamReader(resp_object.response.getEntity().getContent()));
                    StringBuffer sb = new StringBuffer("");
                    String l = "";
                    String nl = System.getProperty("line.separator");
                    while ((l = in.readLine()) !=null){
                        sb.append(l + nl);
                    }
                    in.close();
                    resp_object.data = sb.toString();
                    resp_object.Success=true;


                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    resp_object.error_msg=e.getMessage();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    resp_object.error_msg=e.getMessage();
                } catch (Exception e){

                    e.printStackTrace();
                    resp_object.error_msg=String.valueOf(e.getLocalizedMessage());
                }
                break;
            case "POST":
                Log.d("Url",base_url+Url);
                String params = "?";
                for (int i = 0; i <nameValuePair.size() ; i++) {
                    BasicNameValuePair keyValue = (BasicNameValuePair) nameValuePair.get(i);
                    params = params + keyValue.getName() + "=" + keyValue.getValue()+"&";
                }
                Log.d("check",params);
                postRequest=new HttpPost(base_url+Url+params);
                sharedPref = context.getSharedPreferences("session_token", Context.MODE_PRIVATE);
                token = sharedPref.getString("token","");
                Log.d("token",token);
                try {
//                    GetRequest.addHeader("X-Csrf-Token",token);
                    resp_object.response = httpClient.execute(postRequest);
                    BufferedReader in = new BufferedReader(new InputStreamReader(resp_object.response.getEntity().getContent()));
                    StringBuffer sb = new StringBuffer("");
                    String l = "";
                    String nl = System.getProperty("line.separator");
                    while ((l = in.readLine()) !=null){
                        sb.append(l + nl);
                    }
                    in.close();
                    resp_object.data = sb.toString();
                    resp_object.Success=true;


                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    resp_object.error_msg=e.getMessage();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    resp_object.error_msg=e.getMessage();
                } catch (Exception e){

                    e.printStackTrace();
                    resp_object.error_msg=String.valueOf(e.getLocalizedMessage());
                }
                break;
        }
        Log.d("apidata", resp_object.data);
        Log.d("apierror", String.valueOf(resp_object.error_msg));
        Log.d("apiSuccess", String.valueOf(resp_object.Success));
        return resp_object;

    }

    public static void getToeken(Context context){
        GetRequest=new HttpGet(host_url+"/auth/token");
        try {
            HttpResponse response_token = httpClient.execute(GetRequest);
            BufferedReader in = new BufferedReader(new InputStreamReader(response_token.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) !=null){
                sb.append(l + nl);
            }
            in.close();

            SharedPreferences sharedPref = context.getSharedPreferences("session_token",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("token",sb.toString());
            editor.commit();
            Log.d("token",sb.toString());

            editor.commit();

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e){

            e.printStackTrace();
        }
    }

    public static String[] executeAsyncWebServiceTask(String webServiceUrl, String webServiceMethod){
      String[] result = new String[0];
        new AsyncTaskManager().execute(webServiceUrl);
      return result;
    }
}

class AsyncTaskManager extends AsyncTask<String, apiUtility, apiUtility> {
    protected void onPostExecute(apiUtility result){
        Log.d("cab_fragment","end async with result ");

    }

    @Override
    protected apiUtility doInBackground(String...params) {
        apiUtility apiUtilityResult = new apiUtility(true,null,null,null,null);
        apiUtilityResult = apiUtility.executeApi("GET",params[0],null,null);
        return apiUtilityResult;
    }
}