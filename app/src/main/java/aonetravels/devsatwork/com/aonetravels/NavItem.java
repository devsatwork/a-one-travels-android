package aonetravels.devsatwork.com.aonetravels;

/**
 * Created by supreet on 2/20/2015.
 */
public class NavItem {
    String mTitle;
//    String mSubtitle;
    int mIcon;

    public NavItem(String title, int icon) {
        mTitle = title;
//        mSubtitle = subtitle;
        mIcon = icon;
    }
}