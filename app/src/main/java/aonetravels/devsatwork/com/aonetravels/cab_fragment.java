package aonetravels.devsatwork.com.aonetravels;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by supreet on 2/19/2015.
 */
public class cab_fragment extends Fragment {


    apiUtility util =new apiUtility(false,null,null,null,this.getActivity());
    String response;
    String[] Sources = new String[] { "Select Source" };
    String[] Destinations = new String[] { "Select Destination" };
    JSONArray source_objs;
    JSONArray destination_objs;
    private View cabView = null;
    private EditText source, destination, capacity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        cabView = inflater.inflate(R.layout.cab_fragment, container, false);
        getActionBar().setTitle("Book A Ride");

        // initialize views
        initializeViews();
        Context myContext = this.getActivity();
        util=apiUtility.executeApi("GET","sources",null,this.getActivity());
        if(util.Success){
            try {
                JSONArray reader = new JSONArray(util.data);
                int index=0;
                String[] Sources_array=new String[reader.length()];
                source_objs=reader;
                for(index=0;index<reader.length();index++){
                    JSONObject source_obj = reader.getJSONObject(index);

                    Log.d("reader", source_obj.getString("name"));
                    Sources_array[index]= (String) source_obj.get("name");
                }
                Sources=Sources_array;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Log.d("error", String.valueOf(util.error_msg));
        }
        util=apiUtility.executeApi("GET","sources",null,this.getActivity());
        if(util.Success){
            try {
                JSONArray reader = new JSONArray(util.data);
                int index=0;
                String[] Destinations_array=new String[reader.length()];
                destination_objs=reader;
                for(index=0;index<reader.length();index++){
                    JSONObject destination_obj = reader.getJSONObject(index);

                    Log.d("reader", destination_obj.getString("name"));
                    Destinations_array[index]= (String) destination_obj.get("name");
                }
                Destinations = Destinations_array;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Log.d("error", String.valueOf(util.error_msg));
        }
        final Fragment myFragment = this;
        cabView.findViewById(R.id.buttonBookCab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("email", "fswift@gmail.com"));
                List<NameValuePair> bookingDetails = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("booking_type", "cab"));
                //params.add(new BasicNameValuePair("bookingDetails", bookingDetails));
                util = apiUtility.executeApi("POST", "newBookings", params, myFragment.getActivity());
                if (util.Success) {
                    Intent myIntent = new Intent(cab_fragment.this.getActivity(), MainActivity.class);
                    cab_fragment.this.startActivity(myIntent);
                } else {
                    Log.d("SUCKS", "fails");
                }*/
                if (validateFields()) {
                    Fragment objFragment = null;
                    objFragment = new cab_model_fragment();
                    Bundle args = new Bundle();

                    args.putString("capacity", String.valueOf(capacity.getText()));
                    Log.d("validate", String.valueOf(capacity.getText()));
                    objFragment.setArguments(args);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                            .replace(R.id.container, objFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }

        });

        /*final Spinner source_spinner = (Spinner) view.findViewById(R.id.spinnerCabSource);
        final Spinner destination_spinner = (Spinner) view.findViewById(R.id.spinnerCabDestination);

        ArrayAdapter<String> source_adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, Sources);
        source_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        source_spinner.setAdapter(source_adapter);

        ArrayAdapter<String> destination_adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, Destinations);
        destination_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        destination_spinner.setAdapter(destination_adapter);


        source_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        */
        return cabView;
    }


    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * This function initializes all the view elements
     */
    private void initializeViews() {
        source = (EditText) cabView.findViewById(R.id.editTextSource);
        destination = (EditText) cabView.findViewById(R.id.editTextDestination);
        capacity = (EditText) cabView.findViewById(R.id.editTextCapacity);
    }

    /**
     * This function validates all fields of the view
     * @return - true if validation succeed otherwise false
     */
    private boolean validateFields() {
        if (source.getText().equals(null) || "".equals(source.getText().toString())){
            Toast.makeText(getActivity(), "Enter source station.", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (destination.getText().equals(null) || "".equals(destination.getText().toString())) {
            Toast.makeText(getActivity(), "Enter destination station.", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (!capacity.getText().equals(null) || "".equals(capacity.getText().toString())) {
            Toast.makeText(getActivity(), "Enter capacity.", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (Integer.parseInt(capacity.getText().toString()) < 1) {
            Toast.makeText(getActivity(), "Capacity can't be zero.", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

}


