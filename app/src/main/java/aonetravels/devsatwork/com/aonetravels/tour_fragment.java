package aonetravels.devsatwork.com.aonetravels;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by supreet on 2/19/2015.
 */
public class tour_fragment extends Fragment {

    ArrayList<Tours> ToursList = new ArrayList<Tours>();
    private apiUtility util;
    private ProgressBar spinner;
    ProgressBar progressbar;
    class Tours {
        int id;
        String name;
        String description;
        String image_url;
        String caption;
        ImageView image;
    }

    private Editable capacity;
    int tour_id;
    ToursListAdapter toursListAdapter = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.tour_fragment, container, false);
        //EditText capacityTextBox = (EditText) view.findViewById(R.id.)
        getActionBar().setTitle("Go on New Tours");
        progressbar=(ProgressBar) view.findViewById((R.id.tour_progress));

        final ListView ToursListView  =(ListView) view.findViewById(R.id.TourslistView);
        util=apiUtility.executeApi("GET","getTourPackages",null,this.getActivity());

        if(util.Success){
            try {
                JSONArray reader = new JSONArray(util.data);
                Log.d("toursList", String.valueOf(reader));

                int index=0;
                String[] Sources_array=new String[reader.length()];
                for(index=0;index<reader.length();index++){
                    JSONObject source_obj = reader.getJSONObject(index);
                       Tours tour =new Tours();
                    tour.id=source_obj.getInt("id");
                    tour.caption=source_obj.getString("caption");
                    tour.description=source_obj.getString("description");
                    tour.image_url=source_obj.getString("image");
                    ToursList.add(tour);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final ToursListAdapter adapter =new ToursListAdapter(getActivity().getApplicationContext(),R.layout.tours_row,ToursList);
            ToursListView.setAdapter(adapter);

            ToursListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("pos", String.valueOf(ToursList.get(position).id));
                    final EditText input = new EditText(parent.getContext());
                    tour_id=ToursList.get(position).id;
                    new AlertDialog.Builder(parent.getContext())
                            .setTitle("Capacity?")
                            .setMessage("Enter Capacity")
                            .setView(input)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    capacity = input.getText();
//                                    new BackgroundTasks().execute();
                                    progressbar.setProgress(50);
                                    Fragment objFragment=null;
                                    objFragment=new tour_cab_model_fragment();
                                    Bundle args = new Bundle();
                                    args.putString("capacity", capacity.toString());
                                    args.putString("tour_id", String.valueOf(tour_id));
                                    objFragment.setArguments(args);
                                    FragmentManager fragmentManager = getFragmentManager();

                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                                            .replace(R.id.container,objFragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            // Do nothing.

                        }
                    }).show();
                }
            });
        }


        return view;
    }
    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }
    private class BackgroundTasks  extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {

                Thread.sleep(1000);
                util=apiUtility.executeApi("GET","getTourPackageCabModels/"+tour_id+"/"+capacity,null,getActivity().getApplicationContext());
                if(util.Success){
                    JSONArray reader = null;
                    try {
                        reader = new JSONArray(util.data);
                        Log.d("toursList", String.valueOf(reader));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        //        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

//            Intent i = new Intent(SplashScreen.this,
//                    MainActivity.class);
//            // any info loaded can during splash_show
//            // can be passed to main activity using
//            // below
//            i.putExtra("loaded_info", " ");
//            startActivity(i);
//            finish();
        }
    }
}




