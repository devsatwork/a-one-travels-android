package aonetravels.devsatwork.com.aonetravels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by supreet on 3/10/2015.
 */

class ToursListAdapter extends ArrayAdapter<tour_fragment.Tours>{
    HashMap<tour_fragment.Tours, Integer> mIdMap = new HashMap<>();
    List<tour_fragment.Tours> data;

    tour_fragment.Tours temp;
    public ToursListAdapter(Context context, int textViewResourceId,List<tour_fragment.Tours> tours) {
        super(context,R.layout.tours_row, tours);
        for (int i = 0; i < tours.size(); ++i) {
            mIdMap.put(tours.get(i), i);
        }
        data=tours;
    }
    public static class TourHolder{

        public TextView caption;
        public TextView description;
        public ImageView image;

    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        LayoutInflater inflater=LayoutInflater.from(getContext());
        View ToursView = inflater.inflate(R.layout.tours_row,parent,false);

        TourHolder tour = new TourHolder();
        tour.caption=(TextView) ToursView.findViewById(R.id.tourCaption);
        tour.description=(TextView) ToursView.findViewById(R.id.tourDescription);
        tour.image=(ImageView) ToursView.findViewById(R.id.tourImage);

        temp=null;
        temp= (tour_fragment.Tours) data.get(position);
        tour.caption.setText(temp.caption);
        tour.description.setText(temp.description);


        return ToursView;
    }



}
