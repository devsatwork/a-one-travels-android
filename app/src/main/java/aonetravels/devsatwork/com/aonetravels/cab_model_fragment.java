package aonetravels.devsatwork.com.aonetravels;

import android.support.v4.app.Fragment;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by aditya on 13/4/15.
 */
public class cab_model_fragment extends Fragment {
    private apiUtility util;
    private String string_capacity;
    private ProgressBar spinner;
    ProgressBar progressbar;
    ArrayList<CabModels> ToursList = new ArrayList<CabModels>();
    class CabModels {
        int id;
        String company;
        String model;
        String sub_model;
        String capacity;
        String ac_price;
    }

    ToursListAdapter toursListAdapter = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.tour_cab_model_fragment, container, false);
        getActionBar().setTitle("Select Cab Model");
        progressbar = (ProgressBar) view.findViewById((R.id.cab_progress));
        string_capacity = getArguments().getString("capacity");
        Log.d("re-validate",string_capacity);
        final ListView ToursListView = (ListView) view.findViewById(R.id.CablistView);
        new BackgroundTasks().execute();
        util = apiUtility.executeApi("GET", "carmodels/"+string_capacity, null, this.getActivity());
        if (util.Success) {
            try {
                JSONArray reader = new JSONArray(util.data);
                Log.d("toursList", String.valueOf(reader));

                int index = 0;
                String[] Sources_array = new String[reader.length()];
                for (index = 0; index < reader.length(); index++) {
                    JSONObject source_obj = reader.getJSONObject(index);
                    CabModels tour = new CabModels();
                    tour.id = source_obj.getInt("id");
                    tour.company = source_obj.getString("company");
                    tour.model = source_obj.getString("model");
                    tour.sub_model = source_obj.getString("sub_model");
                    tour.capacity = source_obj.getString("capacity");
                    tour.ac_price = source_obj.getString("fuel_type");
                    ToursList.add(tour);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ToursListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("pos", String.valueOf(ToursList.get(position).id));
                    String myCabId = String.valueOf(ToursList.get(position).id);
//                                    new BackgroundTasks().execute();
                    progressbar.setProgress(75);
                    Fragment objFragment = null;
                    objFragment = new booking_fragment();
                    Bundle args = new Bundle();
                    args.putString("capacity", string_capacity.toString());
                    args.putString("cab_id", myCabId);
                    objFragment.setArguments(args);
                    android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();

                    android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                            .replace(R.id.container, objFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }
        final CabModelListAdapter adapter = new CabModelListAdapter(getActivity().getApplicationContext(), R.layout.cabmodels_row, ToursList);
        ToursListView.setAdapter(adapter);
        return view;
    }
    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }
    private class BackgroundTasks  extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {

                Thread.sleep(1000);
                util=apiUtility.executeApi("GET","getTourPackageCabModels/",null,getActivity().getApplicationContext());
                if(util.Success){
                    JSONArray reader = null;
                    try {
                        reader = new JSONArray(util.data);
                        Log.d("toursList", String.valueOf(reader));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        //        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

//            Intent i = new Intent(SplashScreen.this,
//                    MainActivity.class);
//            // any info loaded can during splash_show
//            // can be passed to main activity using
//            // below
//            i.putExtra("loaded_info", " ");
//            startActivity(i);
//            finish();
        }
    }

    public void setDrawerIndicatorEnabled(boolean b){

    }

}

