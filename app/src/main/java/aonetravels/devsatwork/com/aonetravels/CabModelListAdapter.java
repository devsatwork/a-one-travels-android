package aonetravels.devsatwork.com.aonetravels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by aditya on 13/4/15.
 */
public class CabModelListAdapter extends ArrayAdapter<cab_model_fragment.CabModels> {
    HashMap<cab_model_fragment.CabModels, Integer> mIdMap = new HashMap<>();
    List<cab_model_fragment.CabModels> data;

    cab_model_fragment.CabModels temp;
    public CabModelListAdapter(Context context, int textViewResourceId,List<cab_model_fragment.CabModels> cabs){
        super(context,R.layout.cabmodels_row, cabs);
        for (int i = 0; i < cabs.size(); ++i) {
            mIdMap.put(cabs.get(i), i);
        }
        data=cabs;
    }
    public static class TourHolder{

        public TextView company;
        public TextView model;
        public TextView sub_model;
        public TextView capacity;
        public TextView ac_price;

    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        LayoutInflater inflater=LayoutInflater.from(getContext());
        View ToursView = inflater.inflate(R.layout.tourcabmodels_row,parent,false);

        TourHolder tour = new TourHolder();
        tour.company = (TextView) ToursView.findViewById(R.id.tourcabmodelCompany);
        tour.model = (TextView) ToursView.findViewById(R.id.tourcabmodelModel);
        tour.sub_model = (TextView) ToursView.findViewById(R.id.tourcabmodelSubModel);
        tour.capacity = (TextView) ToursView.findViewById(R.id.tourcabmodelCapacity);
        tour.ac_price = (TextView) ToursView.findViewById(R.id.tourcabmodelAcPrice);

        temp=null;
        temp = (cab_model_fragment.CabModels) data.get(position);
        tour.company.setText(temp.company);
        tour.model.setText(temp.model);
        tour.sub_model.setText(temp.sub_model);
        tour.capacity.setText(temp.capacity);
        tour.ac_price.setText(temp.ac_price);


        return ToursView;
    }



}
