package aonetravels.devsatwork.com.aonetravels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by aditya on 15/3/15.
 */
public class TourCabModelListAdapter extends ArrayAdapter<tour_cab_model_fragment.TourCabModels> {
    HashMap<tour_cab_model_fragment.TourCabModels, Integer> mIdMap = new HashMap<>();
    List<tour_cab_model_fragment.TourCabModels> data;

    tour_cab_model_fragment.TourCabModels temp;
    public TourCabModelListAdapter(Context context, int textViewResourceId,List<tour_cab_model_fragment.TourCabModels> tours) {
        super(context,R.layout.tourcabmodels_row, tours);
        for (int i = 0; i < tours.size(); ++i) {
            mIdMap.put(tours.get(i), i);
        }
        data=tours;
    }
    public static class TourHolder{

        public TextView company;
        public TextView model;
        public TextView sub_model;
        public TextView capacity;
        public TextView ac_price;
        public TextView non_ac_price;

    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        LayoutInflater inflater=LayoutInflater.from(getContext());
        View ToursView = inflater.inflate(R.layout.tourcabmodels_row,parent,false);

        TourHolder tour = new TourHolder();
        tour.company = (TextView) ToursView.findViewById(R.id.tourcabmodelCompany);
        tour.model = (TextView) ToursView.findViewById(R.id.tourcabmodelModel);
        tour.sub_model = (TextView) ToursView.findViewById(R.id.tourcabmodelSubModel);
        tour.capacity = (TextView) ToursView.findViewById(R.id.tourcabmodelCapacity);
        tour.ac_price = (TextView) ToursView.findViewById(R.id.tourcabmodelAcPrice);
        tour.non_ac_price = (TextView) ToursView.findViewById(R.id.tourcabmodelNonAcPrice);

        temp=null;
        temp= (tour_cab_model_fragment.TourCabModels) data.get(position);
        tour.company.setText(temp.company);
        tour.model.setText(temp.model);
        tour.sub_model.setText(temp.sub_model);
        tour.capacity.setText(temp.capacity);
        tour.ac_price.setText(temp.ac_price);
        tour.non_ac_price.setText(temp.non_ac_price);

        return ToursView;
    }



}
