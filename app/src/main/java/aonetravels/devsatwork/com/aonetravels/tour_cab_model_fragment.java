package aonetravels.devsatwork.com.aonetravels;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by supreet on 3/13/2015.
 */
public class tour_cab_model_fragment extends Fragment {
    private apiUtility util;
    private String string_capacity;
    private String string_tour_id;
    private Editable capacity;
    int tour_id;
    private ProgressBar spinner;
    ProgressBar progressbar;
    ArrayList<TourCabModels> ToursList = new ArrayList<TourCabModels>();
    class TourCabModels {
        int id;
        String company;
        String model;
        String sub_model;
        String capacity;
        String ac_price;
        String non_ac_price;
    }

    ToursListAdapter toursListAdapter = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        string_capacity = getArguments().getString("capacity");
        string_tour_id = getArguments().getString("tour_id");
        Log.d("tour_cab_model_fragment","Capacity is "+string_capacity);
        Log.d("tour_cab_model_fragment","Tour_id is "+string_tour_id);
        View view = inflater.inflate(R.layout.tour_cab_model_fragment, container, false);
        getActionBar().setTitle("Select Cab Model");
        progressbar=(ProgressBar) view.findViewById((R.id.cab_progress));

        final ListView ToursListView  =(ListView) view.findViewById(R.id.CablistView);
        new BackgroundTasks().execute();
        util=apiUtility.executeApi("GET","getTourPackageCabModels/"+string_tour_id+"/"+string_capacity,null,this.getActivity());

        if(util.Success) {
            try {
                JSONArray reader = new JSONArray(util.data);
                Log.d("toursList", String.valueOf(reader));

                int index = 0;
                String[] Sources_array = new String[reader.length()];
                for (index = 0; index < reader.length(); index++) {
                    JSONObject source_obj = reader.getJSONObject(index);
                    TourCabModels tour = new TourCabModels();
                    tour.id = source_obj.getInt("id");
                    tour.company = source_obj.getString("company");
                    tour.model = source_obj.getString("model");
                    tour.sub_model = source_obj.getString("sub_model");
                    tour.capacity = source_obj.getString("capacity");
                    tour.ac_price = source_obj.getString("ac_price");
                    tour.non_ac_price = source_obj.getString("non_ac_price");
                    ToursList.add(tour);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final TourCabModelListAdapter adapter = new TourCabModelListAdapter(getActivity().getApplicationContext(), R.layout.tourcabmodels_row, ToursList);
            ToursListView.setAdapter(adapter);

            ToursListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("pos", String.valueOf(ToursList.get(position).id));
                                    String myCabId = String.valueOf(ToursList.get(position).id);
//                                    new BackgroundTasks().execute();
                                    progressbar.setProgress(75);
                                    Fragment objFragment = null;
                                    objFragment = new booking_fragment();
                                    Bundle args = new Bundle();
                                    args.putString("capacity", string_capacity.toString());
                                    args.putString("tour_id", String.valueOf(string_tour_id));
                                    args.putString("cab_id", myCabId);
                                    objFragment.setArguments(args);
                                    android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();

                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                                            .replace(R.id.container, objFragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                }
            });
        }

        //ActionBar myActionBar = getActionBar();
        //myActionBar.setDrawerIndicatorEnabled(false);
        //ActionBarDrawerToggle myDrawerToggle = (ActionBarDrawerToggle) view.findViewById(R.id.navigation_drawer);
        //myActionBar.setDisplayShowHomeEnabled(true);
        return view;
    }
    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }
    private class BackgroundTasks  extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {

                Thread.sleep(1000);
                util=apiUtility.executeApi("GET","getTourPackageCabModels/"+string_tour_id+"/"+string_capacity,null,getActivity().getApplicationContext());
                if(util.Success){
                    JSONArray reader = null;
                    try {
                        reader = new JSONArray(util.data);
                        Log.d("toursList", String.valueOf(reader));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        //        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

//            Intent i = new Intent(SplashScreen.this,
//                    MainActivity.class);
//            // any info loaded can during splash_show
//            // can be passed to main activity using
//            // below
//            i.putExtra("loaded_info", " ");
//            startActivity(i);
//            finish();
        }
    }

    public void setDrawerIndicatorEnabled(boolean b){

    }

}
