package aonetravels.devsatwork.com.aonetravels;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.widget.ActivityChooserView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Snehal on 6/30/2015.
 */
public class Registration extends ActionBarActivity implements View.OnClickListener{
    private EditText textEmail;
    private EditText textPassword;
    private EditText textPasswordConfirm;
    private Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);

        // hide action bar for login page
        getSupportActionBar().hide();

        // call fun to initialize views
        initializeViews();

    }

    private void initializeViews() {
        textEmail = (EditText) findViewById(R.id.editTextSignupEmail);
        textPassword = (EditText) findViewById(R.id.editTextSignupPassword);
        textPasswordConfirm = (EditText) findViewById(R.id.editTextSignupPasswordConfirm);
        signUp = (Button) findViewById(R.id.buttonSignup);
        signUp.setOnClickListener(this);
    }

    private boolean validateFields() {
        if (textEmail.getText().toString().equals(null) || "".equals(textEmail.getText().toString())){
            Toast.makeText(this, "Enter email address.", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (textPassword.getText().toString().equals(null) || textPassword.getText().toString().length() < 6) {
            Toast.makeText(this, "Password must be minimum 6 characters.", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (!textPassword.getText().toString().equals(textPasswordConfirm.getText().toString())) {
            Toast.makeText(this, "Password doesn't match.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (validateFields()){
            Intent myIntent = new Intent(this, MainActivity.class);
            startActivity(myIntent);
        }
    }
}
