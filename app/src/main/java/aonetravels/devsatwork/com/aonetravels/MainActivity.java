package aonetravels.devsatwork.com.aonetravels;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.internal.widget.ActionBarContextView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.zip.Inflater;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    /*private FragmentManager.OnBackStackChangedListener
            mOnBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            setActionBarArrowDependingOnFragmentsBackStack();
        }
    };*/

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private Drawable mIcon;
    private ActionBarDrawerToggle myDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("this","testing log");
        super.onCreate(savedInstanceState);

        //this.getActionBar().setDisplayShowHomeEnabled(true);
        //this.getActionBar().setDisplayHomeAsUpEnabled(true);
        DrawerLayout myDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*myDrawerToggle = new ActionBarDrawerToggle(this, myDrawerLayout ,0,0){

            public void onDrawerClosed(View view) {
                setActionBarArrowDependingOnFragmentsBackStack();
            }

            public void onDrawerOpened(View drawerView) {
                myDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        };
        myDrawerLayout.setDrawerListener(myDrawerToggle);
        getSupportFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);*/
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        /*MenuItem logoutItem = (MenuItem) findViewById(R.id.action_logout);
        if (logoutItem != null) {
            logoutItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    attemptLogout();
                    return false;
                }
            });
        }*/

//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.container, new RegistrationFragment())
//                .commit();

    }
    /*private void setActionBarArrowDependingOnFragmentsBackStack() {
        int backStackEntryCount =
                getSupportFragmentManager().getBackStackEntryCount();
        myDrawerToggle.setDrawerIndicatorEnabled(backStackEntryCount == 0);
    }*/


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment objFragment=null;
        switch (position){
            case 0:
                objFragment=new home_fragment();
                break;
            case 1:
                objFragment=new tour_fragment();
                break;
            case 2:
                objFragment=new cab_fragment();
                break;
            case 3:
                objFragment=new hotel_fragment();
                break;
            case 4:
                objFragment=new flight_fragment();
                break;
        }

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, objFragment)
                .commit();

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle=getString(R.string.app_name);
                mIcon=getResources().getDrawable(R.drawable.ic_home_white);
                break;
            case 2:
                mTitle = getString(R.string.title_tour);
                mIcon=getResources().getDrawable(R.drawable.ic_car_white);
                break;
            case 3:
                mTitle = getString(R.string.title_cabs);
                mIcon=getResources().getDrawable(R.drawable.ic_car_white);
                break;
            case 4:
                mTitle = getString(R.string.title_hotels);
                mIcon=getResources().getDrawable(R.drawable.ic_hotel_white);
                break;
            case 5:
                mTitle = getString(R.string.title_flights);
                mIcon=getResources().getDrawable(R.drawable.ic_plane_white);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void goToFrame(View v){
        Log.d("this", String.valueOf(v.getId()== R.id.home_flight));
        Log.v("this","creating tests");


        switch (v.getId()){
            case R.id.home_cabs:
                this.onNavigationDrawerItemSelected(1);
                android.app.ActionBar actionbar = getActionBar();
                break;
            case R.id.home_hotel:
                this.onNavigationDrawerItemSelected(2);
                this.onNavigationDrawerItemSelected(2);
                break;
            case R.id.home_flight:
                this.onNavigationDrawerItemSelected(3);
                this.onNavigationDrawerItemSelected(3);
                break;
        }
    }

    public void onButtonClick(View v){
        Log.v("this","print this message");
    }



    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_one_way:
                if (checked)
                    Log.d("one_way","one_way");
                // Pirates are the best
                break;
            case R.id.radio_round_trip:
                if (checked) {
                    Log.d("round","round");
                }
                break;
            // Ninjas rule
        }
    }

    public void attemptLogout(MenuItem logout){
        Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
        MainActivity.this.startActivity(myIntent);
        Log.d("Login Activity","Will logout now");
    }

}


