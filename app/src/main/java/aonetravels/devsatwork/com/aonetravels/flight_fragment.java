package aonetravels.devsatwork.com.aonetravels;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by supreet on 2/19/2015.
 */
public class flight_fragment extends Fragment {
    private String trip_type ="one_way";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        final View view = inflater.inflate(R.layout.flight_fragment, container, false);
        getActionBar().setTitle("Travel Around the World");
        RadioGroup radio_trip_type = (RadioGroup) view.findViewById(R.id.trip_type);
        radio_trip_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_one_way:
                        trip_type="one_way";
                        break;
                    case  R.id.radio_round_trip:
                        trip_type="round_trip";
                        break;
                }

                setTripType(trip_type,view);
            }
        });


        setTripType(trip_type, view);

        setDatePickerViews(view, inflater, container);


        return view;
    }


    private void setDatePickerViews(View view, LayoutInflater inflater, ViewGroup container) {


        RelativeLayout RDepart = (RelativeLayout) view.findViewById(R.id.depart_date_time_view);
        View view_depart= inflater.inflate(R.layout.date_time_setter, container, false);
        RDepart.addView(view_depart);


        TextView depart_journey_type=(TextView) view_depart.findViewById(R.id.journey_type);
        TextView depart_journey_day=(TextView) view_depart.findViewById(R.id.journey_day);
        TextView depart_journey_month_year=(TextView) view_depart.findViewById(R.id.journey_month_year);
        TextView depart_journey_time=(TextView) view_depart.findViewById(R.id.journey_time);
        TextView depart_journey_date=(TextView) view_depart.findViewById(R.id.journey_date);

        depart_journey_type.setText("Depart");
        TimeZone utc = TimeZone.getTimeZone("UTC");

        Calendar now = Calendar.getInstance(utc);
//        Date today = new Date();
        Log.d("now", String.valueOf(now));
// EEE gives short day names, EEEE would be full length.
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE", Locale.US);

        depart_journey_day.setText(dateFormat.format(now.getTime()));

        dateFormat = new SimpleDateFormat("MMM");
        String month = dateFormat.format(now.getTime());
        dateFormat = new SimpleDateFormat("yy");
        String year = dateFormat.format(now.getTime());
        depart_journey_month_year.setText(month+'\''+year);

        dateFormat = new SimpleDateFormat("dd");
        depart_journey_date.setText(dateFormat.format(now.getTime()));
        dateFormat = new SimpleDateFormat("hh:mm aa");
        depart_journey_time.setText(dateFormat.format(now.getTime()));

        RelativeLayout RReturn = (RelativeLayout) view.findViewById(R.id.return_date_time_view);
        View view_return= inflater.inflate(R.layout.date_time_setter, container, false);
        RReturn.addView(view_return);

    }

    private void setTripType(String trip_type,View view) {
        RadioGroup radio_trip_type = (RadioGroup) view.findViewById(R.id.trip_type);

        if(trip_type.equals("one_way")){
            radio_trip_type.check(R.id.radio_one_way);
        } else {
            radio_trip_type.check(R.id.radio_round_trip);
        }
    }
    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }
}
