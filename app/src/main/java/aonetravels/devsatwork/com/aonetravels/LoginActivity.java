package aonetravels.devsatwork.com.aonetravels;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import java.util.zip.Inflater;


public class LoginActivity extends ActionBarActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        RegistrationFragment.RegistrationFragmentListener {

    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private static final int RC_SIGN_IN = 0;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    private View myTransitionView;

    private EditText emailAddress;
    private EditText password;
    private Button login;
    private SignInButton googleSignButton;
    private TextView newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // hide action bar for login page
        getSupportActionBar().hide();

        // initializing view elements
        initializeViews();


        googleSignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.sign_in_button
                        && !mGoogleApiClient.isConnecting()) {
                    mSignInClicked = true;
                    resolveSignInError();
                }
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

//BEFORE
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);
//        final Context myContext = this;
//        final RelativeLayout myLoadBaseRelativeLayout = (RelativeLayout) findViewById(R.id.relativeLayoutLoadBase);
//        loadLayout("load",myContext, myLoadBaseRelativeLayout);
//
//        Button loadLoginButton = (Button) findViewById(R.id.buttonLoadLogin);
//        Button loadSignupButton = (Button) findViewById(R.id.buttonLoadSignup);
////        Button loadGooglePlusLogin = (Button) findViewById(R.id.buttonLoadGooglePlus);
//        myTransitionView = (View) findViewById(R.id.relativeLayoutLogin);
//        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (v.getId() == R.id.sign_in_button
//                        && !mGoogleApiClient.isConnecting()) {
//                    mSignInClicked = true;
//                    resolveSignInError();
//                }
//            }
//        });
//
//
//        loadLoginButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("Login Activity","inside Login button click Listener");
//                loadLayout("login",myContext, myLoadBaseRelativeLayout);
//            }
//        });
//        loadSignupButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("Login Activity","inside Load Signup Button Click Listener");
//                loadLayout("signup",myContext, myLoadBaseRelativeLayout);
//            }
//        });
//        //loadGooglePlusLogin.setOnClickListener(new View.OnClickListener() {
//            //`@Override
//          //  public void onClick(View v) {
//            //    Log.d("Login Activiy","inside Load Google Plus Login Button Click Listener");
//                //loadLayout("google");
//            //}
//        //});
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(Plus.API)
//                .addScope(Plus.SCOPE_PLUS_LOGIN)
//                .build();
    }

    private void initializeViews() {
        emailAddress = (EditText) findViewById(R.id.editTextLoginEmail);
        password = (EditText) findViewById(R.id.editTextLoginPassword);
        login = (Button) findViewById(R.id.buttonLogin);

        login.setOnClickListener(this);
        googleSignButton = (SignInButton) findViewById(R.id.sign_in_button);

        newUser = (TextView) findViewById(R.id.new_user);
        newUser.setOnClickListener(this);
    }

    private boolean validate() {
        if (emailAddress.getText().toString().equals(null) || "".equals(emailAddress.getText().toString())){
            Toast.makeText(this, "Enter email address.", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (password.getText().toString().equals(null) || password.getText().toString().length() < 6) {
            Toast.makeText(this, "Password must be minimum 6 characters.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    @Override
    public void onClick(View view) {
        if (view == login && validate()) {
            Intent myIntent = new Intent(this, MainActivity.class);
            startActivity(myIntent);
        }
        else if (view == newUser) {
            Intent registrationIntent = new Intent(this, Registration.class);
            startActivity(registrationIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


//    private void loadLayout(String layoutName, final Context myContext, final RelativeLayout myLoadBaseRelativeLayout){
//    private void loadLayout(String layoutName, final Context myContext, final RelativeLayout myLoadBaseRelativeLayout){
//        LayoutInflater myInflater = LayoutInflater.from(myContext);
//        myLoadBaseRelativeLayout.removeAllViews();
//        final View myView;
////        TransitionManager.beginDelayedTransition((android.view.ViewGroup) myTransitionView);
//        switch(layoutName){
//            case "load":
//                myView = myInflater.inflate(R.layout.load_layout, null);
//                myLoadBaseRelativeLayout.addView(myView);
//                Button loadLoginButton = (Button) findViewById(R.id.buttonLoadLogin);
//                Button loadSignupButton = (Button) findViewById(R.id.buttonLoadSignup);
//                loadLoginButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Log.d("Login Activity","inside Login button click Listener");
//                        loadLayout("login",myContext, myLoadBaseRelativeLayout);
//                    }
//                });
//                loadSignupButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Log.d("Login Activity","inside Load Signup Button Click Listener");
//                        loadLayout("signup",myContext, myLoadBaseRelativeLayout);
//                    }
//                });
//                break;
//            case "signup":
//                myView = myInflater.inflate(R.layout.signup_layout, null);
//                myLoadBaseRelativeLayout.addView(myView);
//                Button loadSignupCancelButton = (Button) findViewById(R.id.buttonSignupCancel);
//                loadSignupCancelButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        loadLayout("load",myContext,myLoadBaseRelativeLayout);
//                    }
//                });
//                Button signupButton = (Button) findViewById(R.id.buttonSignup);
//                signupButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        attemptSignup(myView);
//                    }
//                });
//                break;
//            case "login":
//                myView = myInflater.inflate(R.layout.login_layout, null);
//                myLoadBaseRelativeLayout.addView(myView);
//                Button loadLoginCancelButton = (Button) findViewById(R.id.buttonLoginCancel);
//                loadLoginCancelButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        loadLayout("load",myContext,myLoadBaseRelativeLayout);
//                    }
//                });
//                Button loginButton = (Button) findViewById(R.id.buttonLogin);
//                loginButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        attemptLogin(myView);
//                    }
//                });
//                break;
//            default:
//                Log.d("Login Activity","Wrong element in Switch");
//        }
//
//
//    }

    private boolean validateEmailAddress(String validateText){

        return true;
    }

    private boolean validatePassword(String password, String confirmPassword) {
        return true;
    }

    private void attemptSignup(View myView){
        EditText textEmail = (EditText) findViewById(R.id.editTextSignupEmail);
        EditText textPassword = (EditText) findViewById(R.id.editTextSignupPassword);
        EditText textPasswordConfirm = (EditText) findViewById(R.id.editTextSignupPasswordConfirm);
        String signupEmail = String.valueOf(textEmail.getText());
        String signupPassword = String.valueOf(textPassword.getText());
        String signupPasswordConfirm = String.valueOf(textPasswordConfirm.getText());
        if (validateEmailAddress(signupEmail) && validatePassword(signupPassword, signupPasswordConfirm)){
            Intent myIntent = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(myIntent);
        }
    }

    private void attemptLogin(View myView){
        EditText textEmail = (EditText) findViewById(R.id.editTextLoginEmail);
        EditText textPassword = (EditText) findViewById(R.id.editTextLoginPassword);
        String loginEmail = String.valueOf(textEmail.getText());
        String loginPassword = String.valueOf(textPassword.getText());
        Log.d("login Activity.java","attempting logout");
        if(validateEmailAddress(loginEmail) && validatePassword(loginPassword, loginPassword)){
            Intent myIntent = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(myIntent);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }
    public void onConnectionFailed(ConnectionResult result) {
        if (!mIntentInProgress) {
            // Store the ConnectionResult so that we can use it later when the user clicks
            // 'sign-in'.
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }

    }
    @Override
    public void onConnected(Bundle connectionHint) {
        mSignInClicked = false;
        Intent myIntent = new Intent(LoginActivity.this,MainActivity.class);
        //myIntent.putExtra()
        startActivity(myIntent);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void registerUser() {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }
}
