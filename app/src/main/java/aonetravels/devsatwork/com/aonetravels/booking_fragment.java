package aonetravels.devsatwork.com.aonetravels;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class booking_fragment extends Fragment {
    private apiUtility util;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.booking_fragment,container,false);
        getActionBar().setTitle("Booking Details");
        final Fragment myFragment = this;
        view.findViewById(R.id.buttonBook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("email", "fswift@gmail.com"));
                List<NameValuePair> bookingDetails = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("booking_type", "tours"));
                //params.add(new BasicNameValuePair("bookingDetails", bookingDetails));
                util=apiUtility.executeApi("POST","newBookings",params,myFragment.getActivity());
                if(util.Success){
                    Intent myIntent = new Intent(booking_fragment.this.getActivity(), MainActivity.class);
                    booking_fragment.this.startActivity(myIntent);
                }
                else{
                    Log.d("SUCKS","fails");
                }
            }

        });
        return view;
    }



    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }


    private class BackgroundTasks  extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {

                Thread.sleep(1000);

            }
            catch(Exception e){

            }
            return null;
        }

        //        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

//            Intent i = new Intent(SplashScreen.this,
//                    MainActivity.class);
//            // any info loaded can during splash_show
//            // can be passed to main activity using
//            // below
//            i.putExtra("loaded_info", " ");
//            startActivity(i);
//            finish();
        }
    }
}




